import java.math.*;
import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 * Kasutatud allikad:
 * https://stackoverflow.com/questions/4234985/how-to-for-each-the-hashmap
 * https://enos.itcollege.ee/~jpoial/algoritmid/Huffman/Huffman.java
 * https://stackoverflow.com/questions/17727310/convert-binary-string-to-byte-array
 */
public class Huffman {

    private Integer orig;
    private Huffman left;
    private Huffman right;
    private String symbolByte;
    private Integer frequency;
    private String bits;
    private Integer hlength;

    /**
     * Constructor to build the Huffman code for a given bytearray.
     *
     * @param original source data
     */
    Huffman(byte[] original) {
        if (original.length == 0) {
            throw new RuntimeException("Original data can't be empty byte array!");
        }
        orig = original.length;
        left = null;
        right = null;
        symbolByte = null;
        frequency = null;
        bits = null;
        hlength = null;
    }

    /**
     * Length of encoded data in bits.
     *
     * @return number of bits
     */
    public int bitLength() {
        if (this.hlength != null) {
            return hlength;
        }
        return 0;
    }

    public void generateBits(Huffman man) {
        if (man.left != null) {
            Huffman nextLeft = man.left;
            if (man.bits == null) {
                nextLeft.bits = "0";
            } else {
                nextLeft.bits = man.bits + "0";
            }

            if (nextLeft.left != null) {
                generateBits(nextLeft);
            }
            if (nextLeft.right != null) {
                generateBits(nextLeft);
            }
        }
        if (man.right != null) {
            Huffman nextRight = man.right;
            if (man.bits == null) {
                nextRight.bits = "1";
            } else {
                nextRight.bits = man.bits + "1";
            }

            if (nextRight.left != null) {
                generateBits(nextRight);
            }
            if (nextRight.right != null) {
                generateBits(nextRight);
            }
        }
    }

    public String getBits(Huffman root, String symbol) {
        if (root.symbolByte != null) {
            if (root.symbolByte.equals(symbol)) {
                return root.bits;
            }
        }

        if (root.left != null) {

            String res = getBits(root.left, symbol);
            if (!res.equals("")) {
                return res;
            }
        }

        if (root.right != null) {

            String res = getBits(root.right, symbol);
            if (!res.equals("")) {
                return res;
            }
        }
        return "";
    }


    public String getSymbolByte(String bits) {
        if (this.bits != null) {
            if (this.bits.equals(bits)) {
                if (this.left != null && this.right != null) {
                    return "addNext";
                }
            }
        }
        if (this.symbolByte != null) {
            if (this.bits.equals(bits)) {
                return this.symbolByte;
            }
        }

        if (this.left != null) {

            String res = this.left.getSymbolByte(bits);
            if (!res.equals("")) {
                return res;
            }
        }

        if (this.right != null) {

            String res = this.right.getSymbolByte(bits);
            if (!res.equals("")) {
                return res;
            }
        }
        return "";
    }

    /**
     * Encoding the byte array using this prefixcode.
     *
     * @param origData original data
     * @return encoded data
     */
    public byte[] encode(byte[] origData) {
        if (origData.length == 0) {
            throw new RuntimeException("Original data can't be empty byte array!");
        }
        HashMap<String, Integer> dict = new HashMap<String, Integer>();
        for (byte bait : origData) {
            if (dict.containsKey(String.valueOf(bait))) {
                Integer i = dict.get(String.valueOf(bait));
                dict.replace(String.valueOf(bait), i + 1);
            } else {
                dict.put(String.valueOf(bait), 1);
            }
        }
        PriorityQueue<Huffman> pq = new PriorityQueue<Huffman>(Comparator.comparingInt(l -> l.frequency));
        for (Map.Entry<String, Integer> entry : dict.entrySet()) {
            Huffman leaf = new Huffman(origData);
            leaf.symbolByte = entry.getKey();
            leaf.frequency = entry.getValue();
            pq.add(leaf);
        }

        while (pq.size() >= 2) {
            Huffman left = pq.poll();
            Huffman right = pq.poll();
            Huffman tipp = new Huffman(origData);
            tipp.frequency = left.frequency + right.frequency;
            tipp.left = left;
            tipp.right = right;
            pq.add(tipp);
        }

        Huffman k = pq.poll();
        generateBits(k);
        if (k.right == null && k.left == null) {
            k.bits = "0";
        }

        StringBuffer encodedString = new StringBuffer();
        for (byte bait : origData) {
            String s = getBits(k, String.valueOf(bait));
            encodedString.append(s);
        }
        k.hlength = encodedString.length();

        // Make "this" equal k so that I can access it within decode.
        this.orig = k.orig;
        this.left = k.left;
        this.right = k.right;
        this.symbolByte = k.symbolByte;
        this.frequency = k.frequency;
        this.bits = k.bits;
        this.hlength = k.hlength;

        int len;
        if (encodedString.length() % 8 != 0) {
            len = encodedString.length() / 8 + 1;
        } else {
            len = encodedString.length() / 8;
        }
        int test = len * 8 - encodedString.length();


        for (int i = 0; i < test; i++) {
            encodedString.append("0");
        }

        byte[] bytes = new BigInteger(encodedString.toString(), 2).toByteArray();

        return bytes;
    }

    /**
     * Decoding the byte array using this prefixcode.
     *
     * @param encodedData encoded data
     * @return decoded data (hopefully identical to original)
     */
    public byte[] decode(byte[] encodedData) {
        int nrOfBits = this.bitLength();
        StringBuffer bits = new StringBuffer();

        for (byte bait : encodedData) {
            bits.append(String.format("%8s", Integer.toBinaryString(bait & 0xFF)).replace(' ', '0'));
        }

        bits.delete(nrOfBits, bits.length());


        String subStr = "";
        int bytePos = 0;
        byte[] res = new byte[this.orig];


        for (int i = 0; i < bits.length(); i++) {
            subStr += bits.charAt(i);

            String byteValue = getSymbolByte(subStr);
            if (!byteValue.equals("addNext")) {
                res[bytePos] = Byte.parseByte(byteValue);
                bytePos++;
                subStr = "";
            }
        }

        return res;
    }

    /**
     * Main method.
     */
    public static void main(String[] params) {
        String tekst = "";
        byte[] orig = tekst.getBytes();
        Huffman huf = new Huffman(orig);
        byte[] kood = huf.encode(orig);
        byte[] orig2 = huf.decode(kood);
        // must be equal: orig, orig2
        System.out.println(Arrays.equals(orig, orig2));
        int lngth = huf.bitLength();
        System.out.println("Length of encoded data in bits: " + lngth);
        // TODO!!! Your tests here!
    }
}

